#! /usr/bin/env node

// Don't forget to set permission like chmod +x ./streams.js

const fs = require('fs');
const through2 = require('through2');

const argv = require('minimist')(process.argv.slice(2));
const csv = require('csvtojson');
const promisify = require('util').promisify;
const {google} = require('googleapis');
const keys = require('../oauth2_keys.js');

const readline = require('readline');

const SCOPES = ['https://www.googleapis.com/auth/drive.metadata.readonly'];

function authorize(callback) {
  const {clientSecret, clientID, url} = keys;
  const oAuth2Client = new google.auth.OAuth2(
    clientSecret, clientID, url);

  // Check if we have previously stored a token.
  // fs.readFile(TOKEN_PATH, (err, token) => {
  //   if (err) return getAccessToken(oAuth2Client, callback);
  //   oAuth2Client.setCredentials(JSON.parse(token));
  //   callback(oAuth2Client);
  //});
}

function getAccessToken(oAuth2Client, callback) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
  });
  console.log('Authorize this app by visiting this url:', authUrl);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  rl.question('Enter the code from that page here: ', (code) => {
    rl.close();
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return callback(err);
      oAuth2Client.setCredentials(token);
      // Store the token to disk for later program executions
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
        if (err) console.error(err);
        console.log('Token stored to', TOKEN_PATH);
      });
      callback(oAuth2Client);
    });
  });
}

function listFiles(auth) {
  const drive = google.drive({version: 'v3', auth});

  const fileId = 'ACFrOgDSwgmweImNgyuE0nioqDb8t14Kr1OeHDtvwNFlZnOt4EulAb2QSJ5nh_XJ230ST1AyXxdfrJg8fr5bsOum9a2Qluer5ivadsQueLimluhKQfg-M7ZgM6oVXDo%3D&authuse';
  const dest = fs.createWriteStream('/tmp/remote.css');
  drive.files.get({
    fileId: fileId,
    alt: 'media'
  })
    .on('end', function () {
      console.log('Done');
    })
    .on('error', function (err) {
      console.log('Error during download', err);
    })
    .pipe(dest);
}

function downloadFile() {
  authorize(listFiles);
}

const callHelp = () => {
  console.log(`
     /*
     * 
     * **** CODE WHICH IMPLEMENTS COMMAND LINE INTERACTION ****
     *
     */
    
    ./streams.js --action=outputFile --file=users.csv 
    ./streams.js --action=convertFromFile --file=users.csv
    ./streams.js --action=transform textToTransform
    ./streams.js --action=reverse
    
    ./streams.js -a outputFile -f users.csv
    ./streams.js --help
    ./streams.js -h
    ./streams.js --action=cssBundler --path=./assets/css
    ./streams.js --action=cssBundler -p ./assets/css
  `);
};

const readFile = (path, filepath, remote) => {
  console.log('** path, ', path, "filepath", filepath);

  const readableStream = fs.createReadStream(filepath);
  let data = '';

  readableStream.setEncoding('utf8');
  readableStream.on('data', function(chunk) {

    data+=chunk.replace(/\s/g,'');
    console.log('chunk', chunk.replace(/\s/g,''));
  });

  readableStream.on('end', function() {
    fs.writeFile(`${path}/bundle.css`, data,  {'flag':'a'},  function(err) {
      if (err) {
        return console.error(err);
      }
    });
  });
}

const getFiles = (path) => {
  return promisify(fs.readdir)(path).then((filenames) => {
    filenames.map((filename, index) => {
      const filepath = path + '/'+ filename;

      readFile(path, filepath);
      if ( index === filenames.length-1) {
        const remotePath = `https://drive.google.com/viewerng/text?id=ACFrOgDSwgmweImNgyuE0nioqDb8t14Kr1OeHDtvwNFlZnOt4EulAb2QSJ5nh_XJ230ST1AyXxdfrJg8fr5bsOum9a2Qluer5ivadsQueLimluhKQfg-M7ZgM6oVXDo%3D&authuser=0&page=0`;
        downloadFile(remotePath)
      }
    })
  })
}

const cssBundler = (path) => {
  getFiles(path)
}

const reverse = () => {
  process.stdout.write(`Type some string to reverse \n\n`);
  process.stdout.write('>>');

  process.stdin.on('data', (data)=>{
    process.stdout.write('\n' + data.toString().trim().split('').reverse().join()+'\n');
    process.exit();
  });
}

const transform = (passData) => {
    process.stdout.write('\n' + passData.trim().toUpperCase()+'\n');
    process.exit();
}

const outputFile = (file)=> {
  let data = '';
  if (!/.csv\b/.test(file)) {
    console.log('Wrong file format.');
    return;
  }
  fs.createReadStream(file)
    .pipe(csv())
    .pipe(through2(function (chunk, enc, callback) {
      data = chunk.toString();
      callback()
    }))
    .on('finish', ()=> {
      process.stdout.write(data);
    })
}

const convertFromFile = (file)=> {
  let data = '';
  if (!/.csv\b/.test(file)) {
    console.log('Wrong file format.');
    return;
  }
  fs.createReadStream(file)
    .pipe(csv())
    .pipe(through2(function (chunk, enc, callback) {
      data = chunk.toString();
      callback()
    }))

    .on('finish', ()=> {
      process.stdout.write(data);
    })
}

const convertToFile = (file)=> {
  if (file.indexOf('.csv') == -1) {
    console.log('Wrong file format.');
    return;
  }
  fs.createReadStream(file)
    .pipe(csv())
    .pipe(through2(function (chunk, enc, callback) {
      this.push(chunk)
      callback()
    }))

    .pipe(fs.createWriteStream('out.json'))
}

const callActions = (action, secondFlag, secondFlagValue, passData) => {
  if (passData === null && secondFlag === undefined) {
    console.log(`You can send --file key tu run an action`);
  }

  if (action === 'reverse') {
    reverse();
  }

  if (action === 'transform') {
    transform(passData);
  }

  if (action === 'outputFile') {
    outputFile(secondFlagValue);
  }

  if (action === 'convertFromFile') {
    convertFromFile(secondFlagValue);
  }

  if (action === 'convertToFile') {
     convertToFile(secondFlagValue);
  }

  if (action === 'cssBundler') {
    cssBundler(secondFlagValue);
  }
}

const parseCommand = (firstFlag, firstFlagValue, secondFlag, secondFlagValue, passData) => {

  if (firstFlag === 'h' || firstFlag === 'help' ) {
    callHelp();
  } else if (firstFlag === 'a' || firstFlag === 'action') {
    callActions(firstFlagValue, secondFlag, secondFlagValue, passData);
  } else {
    console.log(`
      streams.js util does not contain an action passed or received argument is invalid.
    `);
  }
}

const readCommand = () => {
  const keys = Object.keys(argv);
  const passData = argv[keys[1]].length !== 0 ?  argv[keys[0]][0] : null;
  const firstFlag = Object.keys(argv)[1] || null;
  const firstFlagValue = argv[keys[1]] || null;
  const secondFlag = Object.keys(argv)[2] || null;
  const secondFlagValue = argv[keys[2]] || null;

  Object.keys(argv).length === 1 ?  parseCommand(null) :
    parseCommand(firstFlag, firstFlagValue, secondFlag, secondFlagValue, passData)
}

readCommand ();